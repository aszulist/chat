Instrukcja do pobrania oraz instalacji chatu na komputerze lokalnym (na systemie Linux - preferowana wersja Ubuntu 16.04)

Na początku proszę się upewnić, że jest zainstalowany server Apache wraz z PHP oraz MySQL.

Jeśli server nie jest zainstalowany proszę zainstalować według instrukcji ze strony digitalocean.com

https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

1: Pobieranie projektu z bitbucket:

 - wchodzimy w katalog servera - `cd /var/www/html`
 - pobieramy projekt z bitbucket - `git clone https://aszulist@bitbucket.org/aszulist/chat.git`

 Teraz gdy projekt mamy pobrane przechodzimy do przygotowania projektu

2: Przygotowanie projektu:
 - przechodzimy do katalogu z projektem - `cd /var/www/html/chat`

 - musimy zainstalować Composer-a - https://getcomposer.org/download/
 
 Polecenia do zainstalowania composer jako plik composer.phar w naszym projekcie:

 `php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"`

 `php -r "if (hash_file('SHA384', 'composer-setup.php') === 'aa96f26c2b67226a324c27919f1eb05f21c248b987e6195cad9690d5c1ff713d53020a02ac8c217dbf90a7eacc9d141d') { echo 'Installer verified'; } else { echo  'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"`

 `php composer-setup.php`

 `php -r "unlink('composer-setup.php');"`

 - pobieramy pakiety potrzebne do naszego projektu - `php composer.phar update`

 - podczasz pobierania pakietów zostaniemy poproszeni o podanie parametrów potrzebnych do połączenia z bazą danych

 - tworzymy bazę danych o nazwie podanej podczas konfiguracji - `php bin/console doctrine:database:create`

 - ładujemy schemat naszej bazy danych - `php bin/console doctrine:schema:update --force`

 - gdy mamy zainstalowane pakiery oraz baze danych pobieramy biblioteki z pliku package.json (bootstrap oraz jQuery) - npm install
 
 - po tych krokach potrzebne jest wygenerowanie plików js oraz css naszego projektu - `php bin/console assetic:dump`

3: Uruchamianie servera:

 - będąc w katalogu naszego projektu (cd /var/www/html/chat) uruchamiamy server - `php bin/console server:start`

 Server powinien zostać uruchomiony uruchomiony, a czat powinien być dostępny pod adresem - `http://localhost:8000`

4: Logowanie się do czatu:

 - aby zalogować się jako normalny urzytkownik wystarczy wpisać nick i nacisnąć przycisk logowania

 - aby zalogować się jako ekspert należy wejść na adres czatu wraz z odpowiednim tokenem jako parametr - `http://localhost:8000?token=rsat4KRskSti3HBoe2z4YaB1zGszAXP5QvThOZf9`, podać nick i nacisnąc przycisk

 - aby zalogować się jako moderator należy wejść na adres czatu wraz z odpowiednim tokenem jako parametr - `http://localhost:8000?token=kHGBpa0cvjbbRT5xziIEFcdNnzoyaDB0uIGf0qks`, podać nick i nacisnąc przycisk

W razie problemów proszę o kontakt.