<?php

namespace ChatBundle\Factories;

use ChatBundle\Entity\User;
use Doctrine\ORM\EntityManager;

class UserFactory {

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Creating new user
     *
     * @param $nick
     * @return User
     */
    public function create($nick) {

        $userEntity = new User();
        $userEntity->setNick($nick);
        $userEntity->setDateAdded(new \DateTime());

        $this->em->persist($userEntity);
        $this->em->flush();

        return $userEntity;
    }

}