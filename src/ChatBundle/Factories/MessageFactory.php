<?php

namespace ChatBundle\Factories;

use ChatBundle\Entity\Message;
use ChatBundle\Entity\User;
use ChatBundle\Utils\UserService;
use Doctrine\ORM\EntityManager;

class MessageFactory {

    /**
     * @var EntityManager
     */
    private $em;

    private $userService;

    public function __construct(EntityManager $em, UserService $userService) {
        $this->em = $em;
        $this->userService = $userService;
    }

    /**
     * Creating new message
     *
     * @param User $user
     * @param $content
     * @return Message
     */
    public function create(User $user, $content) {

        $messageEntity = new Message();
        $messageEntity->setContent($content);
        $messageEntity->setDateAdded(new \DateTime());
        $messageEntity->setUser($user);

        if ($this->userService->isExpert() || $this->userService->isModerator()) {
            $messageEntity->setStatus(Message::STATUS_ACCEPTED);
            $messageEntity->setDateAccepted(new \DateTime());
        } else {
            $messageEntity->setStatus(Message::STATUS_NOT_ACCEPTED);
            $messageEntity->setDateAccepted(null);
        }

        $this->em->persist($messageEntity);
        $this->em->flush();

        return $messageEntity;
    }

    /**
     * Setting specific status
     *
     * @param $messageId
     * @param $status
     * @return bool
     */
    public function setMessageStatus($messageId, $status) {

        try {
            $message = $this->em->getRepository('ChatBundle:Message')->find($messageId);
            $message->setStatus($status);
            if ($status == Message::STATUS_ACCEPTED) {
                $message->setDateAccepted(new \DateTime());
            }
            $this->em->persist($message);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

}