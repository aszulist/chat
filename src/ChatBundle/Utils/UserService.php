<?php

namespace ChatBundle\Utils;

use ChatBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class UserService {

    /**
     * @var Session
     */
    private $session;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(Session $session, EntityManager $em) {
        $this->session = $session;
        $this->em = $em;
    }

    /**
     * Setting user data to session
     *
     * @param User $user
     * @param $token
     */
    public function loginUser(User $user, $token) {

        $userRole = $this->getRoleByToken($token);

        $this->session->set('user_id', $user->getId());
        $this->session->set('user_role', $userRole);
    }

    /**
     * Clear session
     */
    public function logoutUser() {
        $this->session->clear();
    }

    /**
     * Checking is user data is in session
     *
     * @return bool
     */
    public function isLogged() {
        return !empty($this->session->get('user_id'));
    }

    /**
     * Getting logged user
     *
     * @return User|null
     */
    public function getLoggedUser() {
        if ($this->isLogged()) {
            return $this->em->getRepository('ChatBundle:User')->find($this->session->get('user_id'));
        }
        return null;
    }

    /**
     * Check is logged user is a normal user
     *
     * @return bool
     */
    public function isNormalUser() {
        return $this->session->get('user_role') == User::ROLE_NORMAL;
    }

    /**
     * Check is logged user is a expert
     *
     * @return bool
     */
    public function isExpert() {
        return $this->session->get('user_role') == User::ROLE_EXPERT;
    }

    /**
     * Check is logged user is a moderator
     *
     * @return bool
     */
    public function isModerator() {
        return $this->session->get('user_role') == User::ROLE_MODERATOR;
    }

    /**
     * Getting role id by token
     *
     * @param $token
     * @return int
     */
    private function getRoleByToken($token) {

        switch ($token) {
            case User::MODERATOR_TOKEN:
                return User::ROLE_MODERATOR;
            case User::EXPERT_TOKEN:
                return User::ROLE_EXPERT;
        }

        return User::ROLE_NORMAL;
    }


}