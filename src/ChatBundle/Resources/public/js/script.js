$(document).ready(function()
{
    // refreshing messages div every 2 sec
    setInterval(function () {
        refreshMessages();
    }, 2000);
});

// Add message button
function saveMessage() {
    var messageFieldId = "#message-content";
    var messageContent = $(messageFieldId).val();
    saveMessageRequest(messageContent);
    refreshMessages();
    $(messageFieldId).val('');
}

// Sending create message request
function saveMessageRequest(messageContent) {
    var createMessagePath = Routing.generate('message_create', { 'content': messageContent });
    $.ajax({
        type: "POST",
        url: createMessagePath,
        success: function (response) {
            return response['status'];
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function acceptMessage(messageId) {
    changeMessageStatus(messageId, 1);
}

function rejectMessage(messageId) {
    changeMessageStatus(messageId, 2);
}

// send changing status request
function changeMessageStatus(messageId, status) {
    var changeMessageStatusPath = Routing.generate('message_change_status', { 'id': messageId, 'status': status });
    $.ajax({
        type: "PUT",
        url: changeMessageStatusPath,
        success: function (response) {
            refreshMessages();
            return response['status'];
        },
        error: function (error) {
            console.log(error);
        }
    });
}

// refresh which contains messages div
function refreshMessages() {
    $("#messages-container").load(location.href+" #messages-container>*","");
}