<?php

namespace ChatBundle\Controller;

use ChatBundle\Entity\User;
use ChatBundle\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller {
    /**
     * Render login form - start page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request) {
        $userService = $this->get('chat.utils.user_service');
        if ($userService->isLogged()) {
            return $this->redirectToRoute('message_index');
        }

        $token = $request->query->get('token');

        $form = $this->generateLoginForm(new User(), $token);

        return $this->render('ChatBundle:User:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Login User and redirect to chat page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request) {

        $userRepository = $this->getDoctrine()->getRepository('ChatBundle:User');

        $user = new User();
        $form = $this->generateLoginForm($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $token = $form->get('token')->getData();
            $userEntity = $userRepository->getUserByNick($user->getNick());

            if (!($userEntity instanceof User)) {
                $userEntity = $this->get('chat.factories.user_factory')->create($user->getNick());
            }

            $this->get('chat.utils.user_service')->loginUser($userEntity, $token);

            return $this->redirectToRoute('message_index');
        }

        return $this->render('ChatBundle:User:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Logout user and back to login page
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction() {
        $userService = $this->get('chat.utils.user_service');
        $userService->logoutUser();

        return $this->redirectToRoute('user_index');
    }

    /**
     * Generate login form
     *
     * @param User $user
     * @param null $token
     * @return \Symfony\Component\Form\Form
     */
    private function generateLoginForm(User $user, $token = null) {

        $form = $this->createForm(LoginType::class, $user, array(
            'action' => $this->generateUrl('user_login'),
            'token' => $token
        ));

        return $form;
    }

}
