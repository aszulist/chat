<?php

namespace ChatBundle\Controller;

use ChatBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends Controller {
    /**
     * Render chat interface
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $userService = $this->get('chat.utils.user_service');
        if (!$userService->isLogged()) {
            return $this->redirectToRoute('user_index');
        }

        $user = $userService->getLoggedUser();
        $isModerator = $userService->isModerator();
        $isExpert = $userService->isExpert();
        $messages = $this->getDoctrine()->getRepository('ChatBundle:Message')
            ->getChatMessages($user, $isModerator, $isExpert);

        return $this->render('ChatBundle:Message:index.html.twig', array(
            'messages' => $messages,
            'isModerator' => $userService->isModerator(),
            'isNormalUser' => $userService->isNormalUser(),
            'user' => $user
        ));
    }

    /**
     * Create message action
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request) {

        if (!$this->get('chat.utils.user_service')->isLogged()) {
            return new JsonResponse(['status' => false]);
        }

        $messageContent = $request->query->get('content');
        $user = $this->get('chat.utils.user_service')->getLoggedUser();

        $this->get('chat.factories.message_factory')->create($user, $messageContent);

        return new JsonResponse(['status' => true]);
    }

    /**
     * Setting message status (accept or reject by moderator)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changeMessageStatusAction(Request $request) {
        $userService = $this->get('chat.utils.user_service');
        if (!$userService->isLogged() || !$userService->isModerator()) {
            return new JsonResponse(['status' => false]);
        }

        // getting request parameters
        $messageId = $request->query->get('id');
        $status = $request->query->get('status');

        if ($this->get('chat.factories.message_factory')->setMessageStatus($messageId, $status)) {
            return new JsonResponse(['status' => true]);
        }
        return new JsonResponse(['status' => false]);
    }

}
