<?php

namespace ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="ChatBundle\Repository\MessageRepository")
 */
class Message {
    const STATUS_NOT_ACCEPTED = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECT = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdded", type="datetime")
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAccepted", type="datetime", nullable=true)
     */
    private $dateAccepted;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Message
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Message
     */
    public function setDateAdded($dateAdded) {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded() {
        return $this->dateAdded;
    }

    /**
     * Set dateAccepted
     *
     * @param \DateTime $dateAccepted
     *
     * @return Message
     */
    public function setDateAccepted($dateAccepted) {
        $this->dateAccepted = $dateAccepted;

        return $this;
    }

    /**
     * Get dateAccepted
     *
     * @return \DateTime
     */
    public function getDateAccepted() {
        return $this->dateAccepted;
    }


    /**
     * Set user
     *
     * @param \ChatBundle\Entity\User $user
     *
     * @return Message
     */
    public function setUser(\ChatBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ChatBundle\Entity\User
     */
    public function getUser() {
        return $this->user;
    }


    /**
     * Check is message is not accepted
     *
     * @return bool
     */
    public function isNotAccepted() {
        return $this->status == Message::STATUS_NOT_ACCEPTED;
    }

    /**
     * Check is message is accepted
     *
     * @return bool
     */
    public function isAccepted() {
        return $this->status == Message::STATUS_ACCEPTED;
    }

    /**
     * Check is message is reject
     *
     * @return bool
     */
    public function isReject() {
        return $this->status == Message::STATUS_REJECT;
    }

}
